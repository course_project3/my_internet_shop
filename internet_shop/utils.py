from .models import Category

menu = [{'title': "About", 'url_name': 'about'},
]


class DataMixin:

    def get_user_context(self, **kwargs):
        context = kwargs

        context['menu'] = menu

        categories = Category.objects.all()
        context['categories'] = categories

        if 'cat_selected' not in context:
            context['cat_selected'] = 0
        return context
