from django.contrib.auth import logout, login
from django.contrib.auth.views import LoginView
from django.shortcuts import render, get_object_or_404, redirect
from django.views.generic import ListView, DetailView, CreateView
from django.urls import reverse_lazy

from .forms import RegisterUserForm, LoginUserForm, CommentForm
from .models import Product, Category
from .utils import DataMixin

from cart.forms import CartAddProductForm


class ProductList(DataMixin, ListView):
    model = Product
    context_object_name = 'products'

    def get_queryset(self):
        category_slug = self.kwargs.get('category_slug')
        products = Product.objects.filter(available=True)
        if category_slug:
            category = get_object_or_404(Category, slug=category_slug)
            products = products.filter(category=category)
        return products

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)

        category = None
        category_slug = self.kwargs.get('category_slug')
        if category_slug:
            category = get_object_or_404(Category, slug=category_slug)
        if category:
            cat_selected = category.pk
            title = 'Category - ' + str(category.name)
        else:
            cat_selected = 0
            title = 'All products'

        c_def = self.get_user_context(title=title, cat_selected=cat_selected)

        return dict(list(context.items()) + list(c_def.items()))


class ProductDetail(DataMixin, DetailView):
    model = Product

    def post(self, request, **kwargs):
        comment_form = CommentForm(data=request.POST)
        product = get_object_or_404(Product, id=kwargs['id'],
                                    slug=kwargs['slug'],)
        if comment_form.is_valid():
            new_comment = comment_form.save(commit=False)
            new_comment.product = product
            new_comment.save()

        return redirect(product.get_absolute_url())

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.POST:
            comment_form = CommentForm(data=self.request.POST)
        else:
            comment_form = CommentForm()

        c_def = self.get_user_context(cart_product_form=CartAddProductForm(),
                                      comments=self.object.comments.filter(active=True),
                                      comment_form=comment_form,)

        return dict(list(context.items()) + list(c_def.items()))


class RegisterUser(DataMixin, CreateView):
    form_class = RegisterUserForm
    template_name = 'internet_shop/register.html'
    success_url = reverse_lazy('shop:login')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        c_def = self.get_user_context(title="Registration")

        return dict(list(context.items()) + list(c_def.items()))

    def form_valid(self, form):
        user = form.save()
        login(self.request, user)
        return redirect('shop:product_list')


class LoginUser(DataMixin, LoginView):
    form_class = LoginUserForm
    template_name = 'internet_shop/login.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        c_def = self.get_user_context(title="Login")
        return dict(list(context.items()) + list(c_def.items()))

    def get_success_url(self):
        return reverse_lazy('shop:product_list')


def logout_user(request):
    logout(request)
    return redirect('shop:login')


# def product_detail(request, id, slug):
#     product = get_object_or_404(Product,
#                                 id=id,
#                                 slug=slug,
#                                 available=True)
#     cart_product_form = CartAddProductForm()
#     return render(request, 'internet_shop/product_detail.html', {'product': product,
#                                                         'cart_product_form': cart_product_form})
