from django.urls import re_path, path
from . import views

"""
urlpatterns = [
    re_path(r'^$', views.ProductList.as_view(), name='product_list'),
    re_path(r'^(?P<category_slug>[-\w]+)/$', views.ProductList.as_view(), name='product_list_by_category'),
    re_path(r'^(?P<id>\d+)/(?P<slug>[-\w]+)/$', views.ProductDetail.as_view(), name='product_detail'),
]
"""

urlpatterns = [
    path('', views.ProductList.as_view(), name='product_list'),
    path('register/', views.RegisterUser.as_view(), name='register'),
    path('login/', views.LoginUser.as_view(), name='login'),
    path('logout/', views.logout_user, name='logout'),
    path('<slug:category_slug>/', views.ProductList.as_view(), name='product_list_by_category'),
    path('<int:id>/<slug:slug>/', views.ProductDetail.as_view(), name='product_detail'),
]

# https://gist.github.com/jduff/567195/eeaab1d5cfcbc033c947ae9c4744fec4e16c0809 'base.css'